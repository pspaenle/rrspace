/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "bivpol.h"

using namespace std;
using namespace NTL;

size_t 
BivPol::degree() const {
  size_t res = 0;
  for (size_t i = 0; i < coeffs.size(); ++i)
    res = max(res, i + max(deg(coeffs[i]), 0)); 
  return res;
}

bool 
BivPol::IsZero() const {
  for (size_t i =0; i < coeffs.size(); ++i)
    if (!NTL::IsZero(coeffs[i]))
      return false;
  return true;
}

ZZ_pX 
BivPol::mod_eval(const ZZ_pX& g, const ZZ_pX& modulus) const {
  assert(modulus != ZZ_pX(0));
  if (deg(modulus) == 0) 
   return ZZ_pX(0); 
  ZZ_pXModulus mod(modulus);
  ZZ_pX power_g(1);
  ZZ_pX res(0);
  for (size_t i = 0; i < coeffs.size(); ++i) {
    res += MulMod(coeffs[i] % mod, power_g, mod);
    MulMod(power_g, power_g, g, mod);
  }
  return res;
}
  
BivPol 
BivPol::diffY() const {
  std::vector<ZZ_pX> coeffs_res;
  for (size_t i = 1; i < coeffs.size(); ++i)
    coeffs_res.push_back(i*coeffs[i]);
  return BivPol(coeffs_res);
}

ZZ_pX
NewtonHenselStep(const BivPol& q, const ZZ_pX& s, const ZZ_pX& modulus) {
  assert(deg(modulus) > 0);
  assert(IsZero(q.mod_eval(s, modulus)));
  ZZ_pX mod2 = sqr(modulus);
  ZZ_pX denom    = q.diffY().mod_eval(s, mod2);
  ZZ_pX num      = q.mod_eval(s, mod2);
  assert(deg(GCD(denom, mod2)) == 0);
  ZZ_pX invdenom = InvMod(denom, mod2);
  ZZ_pX NH_step  = MulMod(num, invdenom, mod2);
  ZZ_pX res      = s - NH_step;
  assert(IsZero(q.mod_eval(res, mod2)));
  return res;
}

ZZ_pX
NewtonHensel(const BivPol& q, const ZZ_pX& s, const ZZ_pX& modulus, std::size_t k) {
  ZZ_pX R    = s;
  ZZ_pX mod2 = modulus;
  for (std::size_t i = 0; i < k; ++i) {
    R = NewtonHenselStep(q, R, mod2);
    mod2 = sqr(mod2);
  }
  return R;
}

ostream& 
operator<<(ostream& o, const BivPol& P) {
  o << "[ ";
  for (size_t i = 0; i <= P.degree_y(); ++i)
    o << P.coeff(i) << " ";
  o << "]";
  return o;
}

ostream& 
PrintMagma(ostream& o, const ZZ_pX& P) {
  o << "(";
  for (size_t i = 0; i < (size_t)deg(P); ++i)
    if (!IsZero(coeff(P, i)))
      o << coeff(P, i) << "*x^" << i << " + ";
  o << coeff(P, deg(P)) << "*x^" << deg(P);
  o << ")";
  return o;
}

ostream& 
PrintMagma(ostream& o, const BivPol& P) {
  for (size_t i = 0; i < P.degree_y(); ++i)
    if (!IsZero(P.coeff(i)))
      PrintMagma(o, P.coeff(i)) << "*y^" << i << " + ";
  PrintMagma(o, P.coeff(P.degree_y())) << "*y^" << P.degree_y();
  return o;
}


istream& 
operator>>(istream& i, BivPol& res) {
  string s = "";
  size_t deg;
  res.coeffs.clear();
  ZZ_pX tmp;
  i >> s;
  i >> deg;
  for (size_t j = 0; j <= deg; ++j) {
    i >> tmp;
    res.coeffs.push_back(tmp);
  }
  i >> s;
  return i;
}
