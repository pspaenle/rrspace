/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

// Class representing a Weil's divisor on a smooth plane curve given by a
// bivariate polynomial which is monic (i.e. of the form Y^k + g(X,Y), with
// deg_Y(g) < k).
// Three further assumptions : 
//   - We assume that no point in the support is at infinity
//   - The tangent at any point in the support should not be vertical 
//     (i.e. X = 0)
//   - The points in the support should have distinct x-coordinates
// All these three assumptions are satisfied in generic coordinates.

#ifndef DIVISOR_H_
#define DIVISOR_H_

#include <NTL/ZZ_pX.h>
#include <assert.h>
#include "curve.h"

class EffectiveDivisor {
 private:
  NTL::ZZ_pX f;
  NTL::ZZ_pX g;
  Curve C;

 public:
  EffectiveDivisor() = delete;
  EffectiveDivisor(const Curve& _C) : f(NTL::ZZ_pX(1)), g(NTL::ZZ_pX(0)), C(_C) {}
  EffectiveDivisor(const Curve& _C, const NTL::ZZ_pX& _f, const NTL::ZZ_pX& _g)
    : f(_f), g(_g), C(_C) {
    assert(!NTL::IsZero(f));
    assert(f == NTL::ZZ_pX(1) || NTL::IsZero(C.get_pdefpol()->mod_eval(g, f)));
  }

  Curve curve()      const { return C; }
  NTL::ZZ_pX get_f() const { return f; }
  NTL::ZZ_pX get_g() const { return g; }
  std::size_t degree() const { return deg(f); }

  friend std::istream& operator>>(std::istream&, EffectiveDivisor&);
};

class Divisor {
 private:
  EffectiveDivisor Dpos;
  EffectiveDivisor Dneg;

 public:
  Divisor() = delete;
  Divisor(const Curve& C) : Dpos(C), Dneg(C) {}
  Divisor(const EffectiveDivisor& _Dpos)
    : Dpos(_Dpos), Dneg(EffectiveDivisor(_Dpos.curve())) {}
  Divisor(const EffectiveDivisor& _Dpos, const EffectiveDivisor& _Dneg)
    : Dpos(_Dpos), Dneg(_Dneg) {
    assert(Dpos.curve() == Dneg.curve());  
  }

  std::size_t degree() const { return (int)Dpos.degree() - (int)Dneg.degree(); }
  EffectiveDivisor get_pos() const { return Dpos; }
  EffectiveDivisor get_neg() const { return Dneg; }
  Curve curve() const { return Dpos.curve(); }

  friend std::istream& operator>>(std::istream&, Divisor&);
};

std::ostream& operator<<(std::ostream&, const EffectiveDivisor&);
std::istream& operator>>(std::istream&, EffectiveDivisor&);
std::ostream& operator<<(std::ostream&, const Divisor&);
std::istream& operator>>(std::istream&, Divisor&);

#endif
