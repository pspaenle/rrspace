/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "divisor.h"

using namespace std;
using namespace NTL;

ostream& 
operator<<(ostream& o, const EffectiveDivisor& D) {
  o << "< " << D.get_f() << " " << D.get_g() << " >";
  return o;
}

istream& 
operator>>(istream& i, EffectiveDivisor& D) {
  string s;
  i >> s >> D.f >> D.g >> s;
  return i;
}

ostream& 
operator<<(ostream& o, const Divisor& D) {
  o << "{ " << D.get_pos() << endl << D.get_neg() << " }";
  return o; 
}

istream& 
operator>>(istream& i, Divisor& D) {
  string s;
  i >> s >> D.Dpos >> D.Dneg >> s;
  return i;
}
