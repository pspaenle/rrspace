/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <iostream>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/extensions/HelperMacros.h>
#include "curve.h"
#include "divisor.h"
#include "divisor_class_group.h"
#include <sstream>

class EllipticCurveTest : public CppUnit::TestFixture { 
 private:
   BivPol *q;
   Curve *C;

 public: 
  void setUp() {
    NTL::ZZ_p::init(NTL::ZZ(1009));
    NTL::ZZ_pX q0, q1, q2;
    // Elliptic curve
    std::stringstream("[304 38 866 317]") >> q0;
    std::stringstream("[483 91 912]") >> q1;
    std::stringstream("[262 839]") >> q2;
    q = new BivPol({q0, q1, q2, NTL::ZZ_pX(1)});
    C = new Curve(q);
  }

  void tearDown() {
    delete C;
    delete q;
  }

  void testGroupLaw() {
    NTL::ZZ_pX fO, gO, fE, gE, f1, g1, f2, g2;
    
    // O = (781 : 643 : 1)
    std::stringstream("[228 1]") >> fO;
    std::stringstream("[643]") >> gO;
    Divisor O(
        EffectiveDivisor(*C, fO, gO),
        EffectiveDivisor(*C));

    // E is the zero divisor
    std::stringstream("[1]") >> fE;
    std::stringstream("[]") >> gE;
    EffectiveDivisor E(*C, fE, gE);

    // P1 = (381 : 777 : 1)
    std::stringstream("[628 1]") >> f1;
    std::stringstream("[777]") >> g1;
    EffectiveDivisor P1(*C, f1, g1);

    // P2 = (769 : 205 : 1)
    std::stringstream("[240 1]") >> f2;
    std::stringstream("[205]") >> g2;
    EffectiveDivisor P2(*C, f2, g2);

    EffectiveDivisor P3 =
      AddReduced(1, O, E, P1, P2);
    
    NTL::ZZ_pX fR, gR;

    // PR = (933 : 189 : 1)
    std::stringstream("[76 1]") >> fR;
    std::stringstream("[189]") >> gR;

    CPPUNIT_ASSERT(P3.get_f() == fR);
    CPPUNIT_ASSERT(P3.get_g() == gR);
  }
};

class SmoothQuarticTest : public CppUnit::TestFixture { 
 private:
   BivPol *q;
   Curve *C;

 public: 
  void setUp() {
    NTL::ZZ_p::init(NTL::ZZ(1009));
    NTL::ZZ_pX q0, q1, q2, q3;
    // Elliptic curve
    std::stringstream("[810 944 972 589 71]") >> q0;
    std::stringstream("[790 206 709 905]") >> q1;
    std::stringstream("[236 969 139]") >> q2;
    std::stringstream("[311 861]") >> q3;
    q = new BivPol({q0, q1, q2, q3, NTL::ZZ_pX(1)});
    C = new Curve(q);
  }

  void tearDown() {
    delete C;
    delete q;
  }

  void testGroupLaw() {
    NTL::ZZ_pX fO, gO, fE, gE, f1, g1, f2, g2;
    
    // O = (502 : 832 : 1)
    std::stringstream("[507 1]") >> fO;
    std::stringstream("[832]") >> gO;
    Divisor O(
        EffectiveDivisor(*C, fO, gO),
        EffectiveDivisor(*C));

    // E is the zero divisor
    std::stringstream("[1]") >> fE;
    std::stringstream("[]") >> gE;
    EffectiveDivisor E(*C, fE, gE);

    // D1
    std::stringstream("[374 74 387 1]") >> f1;
    std::stringstream("[78 130 82]") >> g1;
    EffectiveDivisor D1(*C, f1, g1);

    // D2
    std::stringstream("[752 293 10 1]") >> f2;
    std::stringstream("[17 109 43]") >> g2;
    EffectiveDivisor D2(*C, f2, g2);

    EffectiveDivisor D3 =
      AddReduced(3, O, E, D1, D2);
    
    NTL::ZZ_pX fR, gR;

    // PR
    std::stringstream("[512 804 472 1]") >> fR;
    std::stringstream("[405 263 475]") >> gR;

    CPPUNIT_ASSERT(D3.get_f() == fR);
    CPPUNIT_ASSERT(D3.get_g() == gR);
  }
};

int main() {
  CppUnit::TextUi::TestRunner runner;  
  CppUnit::TestCaller<EllipticCurveTest>* test1 = 
    new CppUnit::TestCaller<EllipticCurveTest>(
      "testRiemannRoch", 
      &EllipticCurveTest::testGroupLaw);
  CppUnit::TestCaller<SmoothQuarticTest>* test2 = 
    new CppUnit::TestCaller<SmoothQuarticTest>(
      "testRiemannRoch", 
      &SmoothQuarticTest::testGroupLaw);
  runner.addTest(test1);
  runner.addTest(test2);
  runner.run();
  return 0;
}
