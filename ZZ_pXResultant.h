/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/* This file has been written by Pierrick Gaudry <pierrick.gaudry@loria.fr> 
 * */

#ifndef NTL_ZZ_pXResultant__H
#define NTL_ZZ_pXResultant__H

#include <NTL/ZZ.h>
#include <NTL/ZZ_p.h>
#include <NTL/ZZ_pX.h>

NTL_OPEN_NNS

void resultantWithSubRes(ZZ_p& rres, ZZ_pX& subres, 
    const ZZ_pX& u, const ZZ_pX& v);

NTL_CLOSE_NNS

#endif


