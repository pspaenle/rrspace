CPPFLAGS=--pedantic -Wall --std=c++11 -O3
LDLIBS=-lntl -lgmpxx -lgmp -lm

all: rrspace class_grp_arith tests

clean:
	rm *.o rrspace class_grp_arith tests

algos.o: bivpol.h divisor.h curve.h algos.h algos.cc

bivpol.o: bivpol.h bivpol.cc

divisor.o: divisor.h curve.h bivpol.h divisor.cc 

divisor_class_group.o: divisor.h curve.h bivpol.h divisor.h divisor_class_group.h divisor_class_group.cc

ZZ_pXResultant.o: ZZ_pXResultant.h ZZ_pXResultant.cc

rrspace: rrspace.cc algos.o bivpol.o ZZ_pXResultant.o divisor.o

class_grp_arith: class_grp_arith.cc algos.o bivpol.o ZZ_pXResultant.o divisor.o divisor_class_group.o

tests: tests.cc algos.o bivpol.o ZZ_pXResultant.o divisor.o divisor_class_group.o
	$(CXX) $(CPPFLAGS) $^ $(LDLIBS) -lcppunit -o $@

.PHONY: all clean cleanall
