/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <iostream>
#include <sstream>
#include "divisor_class_group.h"

using namespace NTL;
using namespace std;

int main() {
  long p;
  BivPol eq_curve;
  cin >> p;
  ZZ_p::init(ZZ(p));
  cin >> eq_curve;
  Curve C(&eq_curve);

  // Singular divisor for nodal curves
  EffectiveDivisor E(C);
  cin >> E;
  
  size_t g;
  cin >> g;
  Divisor O(C);
  cin >> O;

  EffectiveDivisor D1(C), D2(C);
  cin >> D1;
  cin >> D2;

  // The following assertion should be true for a nodal curve
  assert(g == ((C.degree()-1)*(C.degree()-2))/2 - E.degree());

  EffectiveDivisor R = AddReduced(g, O, E, D1, D2);
  
  cout << R << endl;
  return 0;
}

