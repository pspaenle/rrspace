/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

// This file contains routines for computing reduced divisors with respect to a
// degree 1 divisor. It also implements the group law in the divisor class
// group.

#ifndef DIVISOR_CLASS_GROUP_H_
#define DIVISOR_CLASS_GROUP_H_

#include "divisor.h"
#include "algos.h"

// Reduction routine: D1 must be a divisor of degree 0 and D2 must be a divisor
// of degree 1.
// 
// Returns an effective divisor D such that D1 = D - g*D2.
// TODO: not yet tested.

EffectiveDivisor 
DivisorReduction(const Divisor& D1, const Divisor& D2, std::size_t g);

// D degree 1 divisor
EffectiveDivisor
AddReduced(std::size_t g, 
           const Divisor& O, 
           const EffectiveDivisor& E,
           const EffectiveDivisor& D1, 
           const EffectiveDivisor& D2);

#endif
