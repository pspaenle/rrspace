/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "divisor_class_group.h"

EffectiveDivisor 
DivisorReduction(const Divisor& D, const Divisor& O, const EffectiveDivisor& E, std::size_t g) { 
  assert(D.degree() == 0);
  assert(O.degree() == 1);
  assert(D.curve() == O.curve());
  assert(D.curve() == E.curve());

  RRspace rr = RiemannRochBasis(Sum(D, MultiplyByInt(O, g)), E);
  assert(rr.num_basis.size() > 0);
  return PositiveDifference(PrincipalDivisor(D.curve(), rr.num_basis[0], E), rr.Dnum);
}

// D degree 1 divisor
// E divisor representing the nodes
EffectiveDivisor
AddReduced(std::size_t g, 
           const Divisor& O, 
           const EffectiveDivisor& E,
           const EffectiveDivisor& D1, 
           const EffectiveDivisor& D2) {
  assert( O.curve()  == D1.curve());
  assert( O.curve()  == D2.curve());
  assert( O.curve()  == E.curve());
  assert( O.degree() == 1);
  assert(D1.degree() <= g);
  assert(D2.degree() <= g);

  Divisor gO = MultiplyByInt(O, g);
  RRspace rr = RiemannRochBasis(Divisor(Sum(Sum(D1, D2), gO.get_neg()), gO.get_pos()), E);
  assert(rr.num_basis.size() > 0);
  EffectiveDivisor res = PositiveDifference(PrincipalDivisor(O.curve(), rr.num_basis[0], E), rr.Dnum);
  assert(res.degree() == g + E.degree());
  return res;
}
