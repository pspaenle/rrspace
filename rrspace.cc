/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <iostream>
#include <sstream>
#include "algos.h"
#include <unistd.h>

using namespace NTL;
using namespace std;

int main(int argc, char** argv) {
  long p;
  BivPol eq_curve;
  cin >> p;
  ZZ_p::init(ZZ(p));
  cin >> eq_curve;
  Curve C(&eq_curve);

  // Singular divisor for nodal curves
  EffectiveDivisor E(C);
  cin >> E;

  Divisor D(C);
  cin >> D;

  RRspace basisRR = RiemannRochBasis(D, E);

  bool MAGMACHECK_FLAG = false;

  int c;
  while ((c = getopt(argc, argv, "chvo:s:")) != -1)
    switch (c) {
      case 'c':
        MAGMACHECK_FLAG = true;
        break;
      default:
        std::cerr << "option not recognized, aborting." << std::endl;
        abort();
    }
  
  if (!MAGMACHECK_FLAG) {
    cout << "Dimension: " << basisRR.num_basis.size() << endl;
    cout << "Denominator: " << endl;
    PrintMagma(std::cout, basisRR.denom) << endl;
    cout << "Numerators: " << endl;
    for (size_t i = 0; i < basisRR.num_basis.size(); ++i)
      PrintMagma(std::cout, basisRR.num_basis[i]) << endl;
  } else {
    cout << "[" << endl;
    for (size_t i = 0; i < basisRR.num_basis.size(); ++i) {
      cout << "(";
      PrintMagma(std::cout, basisRR.num_basis[i]);
      cout << ")/(";
      PrintMagma(std::cout, basisRR.denom);
      cout << ")";
      if (i != basisRR.num_basis.size()-1)
        cout << ",";
      cout << endl;
    }
    cout << "]" << endl;
  }
  return EXIT_SUCCESS;
}
