/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "algos.h"
#include <utility>
#include <vector>
#include "ZZ_pXResultant.h"

using namespace NTL;
using namespace std;

size_t 
GetInterpolationDegree(size_t deg_curve, size_t deg_div) {
  if ((deg_curve + 1)*deg_curve > 2*deg_div)
    return floor((1+sqrt(1+8*deg_div)-1)/2);
  return floor((double)deg_div/(double)deg_curve+((double)deg_curve-1)/2);
}

// Returns a vector such that the ind-th element is the pair (i, j) which
// corresponds to the ind-th monomial X^i Y^j such that j < deg_curve
vector<pair<size_t, size_t> >
BuildMonomialVector(size_t deg_curve, size_t deg_max) {
  vector<pair<size_t, size_t> > res;
  for (size_t d = 0; d <= deg_max; ++d)
    for (size_t i = 0; i <= min(d, deg_curve - 1); ++i) 
      res.push_back(pair<size_t, size_t>(d-i, i));
  return res;
}

mat_ZZ_p
BuildInterpolationMatrixInDegree(const EffectiveDivisor& D, const EffectiveDivisor& E, size_t deg) {
  vector<ZZ_pX> powers_of_gD;
  vector<ZZ_pX> powers_of_xD;
  powers_of_gD.push_back(ZZ_pX(1));
  powers_of_xD.push_back(ZZ_pX(1));
  ZZ_pXModulus modfD(D.get_f());
  
  ZZ_pX x_pol(1);
  x_pol <<= 1;

  for (size_t i = 0; i <= min(deg, D.curve().degree()-1); ++i) {
    ZZ_pX tmp;
    MulMod(tmp, powers_of_gD.back(), D.get_g(), modfD);
    powers_of_gD.push_back(tmp);
  }
 
  for (size_t i = 0; i <= deg; ++i) {
    ZZ_pX tmp;
    MulMod(tmp, powers_of_xD.back(), x_pol % modfD, modfD);
    powers_of_xD.push_back(tmp);
  }

  auto vec_mons = BuildMonomialVector(D.curve().degree(), deg);
  Vec<Vec<ZZ_p> > vec_vec_res;
  Vec<ZZ_p> tmp_row;
  ZZ_pX tmp_pol;

  for (const auto& p : vec_mons) {
    tmp_row.SetLength(0);
    MulMod(tmp_pol, powers_of_xD[p.first], powers_of_gD[p.second], modfD);
    for (size_t i = 0; i < D.degree(); ++i)
      tmp_row.append(coeff(tmp_pol, i));
    vec_vec_res.append(tmp_row);
  }
  
  if (E.degree() > 0) { // singular case
    vector<ZZ_pX> powers_of_gE;
    vector<ZZ_pX> powers_of_xE;
    powers_of_gE.push_back(ZZ_pX(1));
    powers_of_xE.push_back(ZZ_pX(1));
    ZZ_pXModulus modfE(E.get_f());

    for (size_t i = 0; i <= min(deg, D.curve().degree()-1); ++i) {
      ZZ_pX tmp;
      MulMod(tmp, powers_of_gE.back(), E.get_g(), modfE);
      powers_of_gE.push_back(tmp);
    }

    for (size_t i = 0; i <= deg; ++i) {
      ZZ_pX tmp;
      MulMod(tmp, powers_of_xE.back(), x_pol % modfE, modfE);
      powers_of_xE.push_back(tmp);
    }

    for (size_t i = 0; i < vec_mons.size(); ++i) {
      tmp_row.SetLength(0);
      MulMod(tmp_pol, powers_of_xE[vec_mons[i].first], powers_of_gE[vec_mons[i].second], modfE);
      for (size_t ll = 0; ll < E.degree(); ++ll)
        vec_vec_res[i].append(coeff(tmp_pol, ll));
    }
  }

  mat_ZZ_p res;
  res.SetDims(vec_mons.size(), D.degree()+E.degree());
  MakeMatrix(res, vec_vec_res);

  return res;
}

BivPol 
Interpolate(const EffectiveDivisor& D, const EffectiveDivisor& E) {
  size_t deg_res = GetInterpolationDegree(D.curve().degree(), D.degree() + E.degree());
  mat_ZZ_p M = BuildInterpolationMatrixInDegree(D, E, deg_res);
  mat_ZZ_p kerM;
  kernel(kerM, M);
  mat_ZZ_p randmat;
  randmat.SetDims(1, kerM.NumRows());
  for (size_t i = 0; i < (size_t)kerM.NumRows(); ++i)
    randmat.put(0, i, random_ZZ_p());
  mul(kerM, randmat, kerM);
  vector<ZZ_pX> interp_pol;
  auto vec_mons = BuildMonomialVector(D.curve().degree(), deg_res);
  interp_pol.insert(interp_pol.begin(),
                    min(D.curve().degree(), deg_res + 1),
                    ZZ_pX(0));
  for (size_t i = 0; i < vec_mons.size(); ++i) {
    pair<size_t, size_t> p = vec_mons[i];
    interp_pol[p.second] += kerM.get(0, i)* (ZZ_pX(1) << p.first);
  }
  while (IsZero(interp_pol.back())) 
    interp_pol.pop_back();
  interp_pol.shrink_to_fit();
  BivPol res(interp_pol);
  assert(!res.IsZero());
  return res;
}

EffectiveDivisor
PrincipalDivisor(const Curve& C, const BivPol& h, const EffectiveDivisor& E) {
  assert(E.curve() == C);
  size_t deg_res = h.degree() * C.degree(); // Bézout bound
  vec_ZZ_p eval_pts;
  for (long i = 0; i <= (long)deg_res; ++i)
    eval_pts.append(ZZ_p(i));
  vec_vec_ZZ_p eval_C, eval_h;
  vec_ZZ_p eval_res, eval_subres0, eval_subres1;
  for (size_t i = 0; i <= C.degree(); ++i)
    eval_C.append(eval(C.coeff(i), eval_pts));
  for (size_t i = 0; i <= h.degree_y(); ++i)
    eval_h.append(eval(h.coeff(i), eval_pts));

  for (size_t i = 0; i <= deg_res; ++i) {
    ZZ_pX C_tmp, h_tmp, subres_tmp;
    ZZ_p res_tmp;

    for (size_t j = 0; j <= C.degree(); ++j)
      C_tmp += eval_C[j][i] * (ZZ_pX(1) << j);
    for (size_t j = 0; j <= h.degree_y(); ++j)
      h_tmp += eval_h[j][i] * (ZZ_pX(1) << j);
    
    resultantWithSubRes(res_tmp, subres_tmp, C_tmp, h_tmp);
    assert(deg(subres_tmp) == 1);
    eval_res.append(res_tmp);
    eval_subres0.append(ConstTerm(subres_tmp));
    eval_subres1.append(LeadCoeff(subres_tmp));
  }

  ZZ_pX res     = interpolate(eval_pts, eval_res);
  ZZ_pX subres0 = interpolate(eval_pts, eval_subres0);
  ZZ_pX subres1 = interpolate(eval_pts, eval_subres1);

  div(res, res, E.get_f());
  div(res, res, E.get_f());
  rem(subres0, subres0, res);
  rem(subres1, subres1, res);

  return EffectiveDivisor(C, res, -MulMod(InvMod(subres1, res), subres0, res));
}

EffectiveDivisor
PositiveDifference(const EffectiveDivisor& D1, const EffectiveDivisor& D2) {
  assert(D1.curve() == D2.curve());
  ZZ_pX f;
  divide(f, D1.get_f(), GCD(D1.get_f(), D2.get_f()));
  return EffectiveDivisor(D1.curve(), f/LeadCoeff(f), D1.get_g() % f);
}

// // This simple sum only works if D1 and D2 do not have any point with the same
// // x-coordinate in their support
// EffectiveDivisor
// SimpleSum(const EffectiveDivisor& D1, const EffectiveDivisor& D2) {
//   assert(D1.curve() == D2.curve());
//   if (deg(D1.get_f()) == 0) 
//     return D2;
//   if (deg(D2.get_f()) == 0) 
//     return D1;
//   NTL::ZZ_pX gcd, a1, a2;
//   XGCD(gcd, a1, a2, D1.get_f(), D2.get_f());
//   assert(deg(gcd) == 0);
// 
//   ZZ_pX new_f = D1.get_f()*D2.get_f();
//   ZZ_pX new_g = (D1.get_g()*a2*D2.get_f() +
//                  D2.get_g()*a1*D1.get_f()) % new_f;
//   assert(new_g % D1.get_f() == D1.get_g());
//   assert(new_g % D2.get_f() == D2.get_g());
//   return EffectiveDivisor(D1.curve(), new_f, new_g);
// }

// This adding function works only if all points in the support are
// non-singular
EffectiveDivisor
Sum(const EffectiveDivisor& D1, const EffectiveDivisor& D2) {
  assert(D1.curve() == D2.curve());
  if (deg(D1.get_f()) == 0) 
    return D2;
  if (deg(D2.get_f()) == 0) 
    return D1;
  NTL::ZZ_pX gcd, a1, a2;
  XGCD(gcd, a1, a2, D1.get_f(), D2.get_f());
  assert((D1.get_g() % gcd) == (D2.get_g() % gcd));

  ZZ_pX new_f = D1.get_f()*D2.get_f();
  ZZ_pX new_g = (D1.get_g()*a2*(D2.get_f()/gcd) +
                 D2.get_g()*a1*(D1.get_f()/gcd)) % (new_f/gcd);

  new_g = NewtonHenselStep(*D1.curve().get_pdefpol(), new_g, new_f/gcd);
  new_g = new_g % new_f;
  assert(new_g % D1.get_f() == D1.get_g());
  assert(new_g % D2.get_f() == D2.get_g());
  return EffectiveDivisor(D1.curve(), new_f, new_g);
}

EffectiveDivisor
MultiplyByInt(const EffectiveDivisor& D, unsigned int k) {
  ZZ_pX _f = D.get_f();
  ZZ_pX _g = D.get_g();
 
  assert(IsZero(D.curve().get_pdefpol()->mod_eval(_g, _f)));

  // Case where D is the zero divisor
  if (deg(_f) == 0) 
    return D;

  int nbsteps;
  frexp((double)k, &nbsteps);
  // TODO: do all computations mod _f
  _g = NewtonHensel(*D.curve().get_pdefpol(), _g, _f, nbsteps);
  power(_f, _f, k);
  return EffectiveDivisor(D.curve(), _f, _g % _f);
}

Divisor
Sum(const Divisor& D1, const Divisor& D2) {
  return Divisor(Sum(D1.get_pos(), D2.get_pos()),
                 Sum(D1.get_neg(), D2.get_neg()));
}

Divisor
MultiplyByInt(const Divisor& D, unsigned int k) {
  return Divisor(MultiplyByInt(D.get_pos(), k), MultiplyByInt(D.get_neg(), k));
}

vector<BivPol>
EffectiveBasisRRinDegree(const EffectiveDivisor& D, const EffectiveDivisor& E, size_t deg) {
  vector<BivPol> res;
  mat_ZZ_p M = BuildInterpolationMatrixInDegree(D, E, deg);
  mat_ZZ_p kerM;
  kernel(kerM, M);
  for (long k = 0; k < kerM.NumRows(); ++k) {
    vector<ZZ_pX> interp_pol;
    auto vec_mons = BuildMonomialVector(D.curve().degree(), deg);
    interp_pol.insert(interp_pol.begin(),
        min(D.curve().degree(), deg + 1),
        ZZ_pX(0));
    for (size_t i = 0; i < vec_mons.size(); ++i) {
      pair<size_t, size_t> p = vec_mons[i];
      interp_pol[p.second] += kerM.get(k, i)*(ZZ_pX(1) << p.first);
    }
    while (IsZero(interp_pol.back())) 
      interp_pol.pop_back();
    interp_pol.shrink_to_fit();
    res.push_back(BivPol(interp_pol));
  }
  return res;
}

RRspace
RiemannRochBasis(const Divisor& D, const EffectiveDivisor& E) {
  assert(D.curve().get_pdefpol()->mod_eval(D.get_pos().get_g(), D.get_pos().get_f()) == 0);
  assert(D.curve().get_pdefpol()->mod_eval(D.get_neg().get_g(), D.get_neg().get_f()) == 0);
  assert(D.curve().get_pdefpol()->mod_eval(E.get_g(), E.get_f()) == 0);
  BivPol h = Interpolate(D.get_pos(), E);
  EffectiveDivisor Dp = PrincipalDivisor(D.curve(), h, E);
  EffectiveDivisor Dp2 = PositiveDifference(Dp, D.get_pos());
  EffectiveDivisor newDneg = Sum(Dp2, D.get_neg());
  return RRspace(h, EffectiveBasisRRinDegree(newDneg, E, h.degree()), newDneg);
}
