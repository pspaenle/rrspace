/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef CURVE_H_
#define CURVE_H_

#include "bivpol.h"

// We always assume that the curve is given in the following Noether form:
// Y^d + sum_{i=0}^{d-1} f_i(X)*Y^i

class Curve {
 private:
  BivPol *pdefpol;

 public:
  Curve() = delete;
  Curve(BivPol* const _pdefpol) : pdefpol(_pdefpol) {
    assert(pdefpol->degree() == pdefpol->degree_y());
    assert(NTL::IsOne(pdefpol->LeadingCoeff()));
  }

  bool operator==(const Curve& C) { return pdefpol == C.pdefpol; }

  std::size_t degree()            const { return pdefpol->degree_y(); }
  NTL::ZZ_pX coeff(std::size_t i) const { return pdefpol->coeff(i); }
  BivPol* get_pdefpol()           const { return pdefpol; }
};

#endif
