/* Common header file for the rrspace software

This file is part of the rrspace project.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

// This file contains routines for constructing a function on a curve of
// prescribed degree having zeros at prescribed places and with prescribed
// multiplicities.

#ifndef INTERPOLATION_DIV_H_
#define INTERPOLATION_DIV_H_

#include <math.h>
#include <NTL/mat_ZZ_p.h>
#include "bivpol.h"
#include "divisor.h"

// Given an effective divisor D, compute a form h on the curve such that (h) >= D
BivPol
Interpolate(const EffectiveDivisor& D, const EffectiveDivisor& E);

NTL::mat_ZZ_p
BuildInterpolationMatrixInDegree(const EffectiveDivisor& D, const EffectiveDivisor& E, std::size_t deg);

// Given a form h on the curve, compute the principal effective divisor (h)
EffectiveDivisor
PrincipalDivisor(const Curve& C, const BivPol& h, const EffectiveDivisor& E);

EffectiveDivisor
PositiveDifference(const EffectiveDivisor& D1, const EffectiveDivisor& D2);

// Add the possibility that the supports are not disjoint
EffectiveDivisor
Sum(const EffectiveDivisor& D1, const EffectiveDivisor& D2);

EffectiveDivisor
MultiplyByInt(const EffectiveDivisor& D, unsigned int k);

// TODO: simplify obtained divisor
Divisor
Sum(const Divisor& D1, const Divisor& D2);

// Divisor
// SimpleSum(const Divisor& D1, const Divisor& D2);

Divisor
MultiplyByInt(const Divisor& D, unsigned int k);

std::vector<BivPol>
EffectiveBasisRRinDegree(const EffectiveDivisor& D, const EffectiveDivisor& E, std::size_t deg);

struct RRspace {
  BivPol denom;
  std::vector<BivPol> num_basis;

  // Residual divisor: (h) - D_+ + D_-
  // Useful for implementing the arithmetic in the Jacobian with reduced
  // representations of divisors.
  EffectiveDivisor Dnum; 

  RRspace(const BivPol& tdenom, const std::vector<BivPol>& tnum_basis, const EffectiveDivisor& tDnum) 
    : denom(tdenom), num_basis(tnum_basis), Dnum(tDnum) {}
};

RRspace
RiemannRochBasis(const Divisor& D, const EffectiveDivisor& E);

#endif
